--Librerias

library ieee; 
use ieee.std_logic_1164.all; 

--Entidad

entity DMR is
  port( 
    X, Y: in std_logic_vector (2 downto 0);
    M, N, P: out std_logic
  );
end entity DMR;

architecture arq_DMR of DMR is
  begin
    M <= (X(2) xnor Y(2)) and (X(1) xnor Y(1)) and (X(0) xnor Y(0));
    N <= (X(2) and (not Y(2))) or ((X(2) xnor Y(2)) and (X(1) and (not Y(1)))) or ((X(2) xnor Y(2)) and (X(1) xnor Y(1)) and (X(0) and (not Y(0)))) ;
    P <= ((not X(2)) and Y(2)) or ((X(2) xnor Y(2)) and ((not X(1)) and Y(1))) or ((X(2) xnor Y(2)) and (X(1) xnor Y(1)) and ((not X(0)) and Y(0))) ;
end arq_DMR;
