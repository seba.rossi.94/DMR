-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 32-bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "04/18/2018 15:41:10"

-- 
-- Device: Altera EP3C5F256C6 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIII;
LIBRARY IEEE;
USE CYCLONEIII.CYCLONEIII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	DMR IS
    PORT (
	X : IN std_logic_vector(2 DOWNTO 0);
	Y : IN std_logic_vector(2 DOWNTO 0);
	M : BUFFER std_logic;
	N : BUFFER std_logic;
	P : BUFFER std_logic
	);
END DMR;

-- Design Ports Information
-- M	=>  Location: PIN_J2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- N	=>  Location: PIN_M7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- P	=>  Location: PIN_K5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- X[0]	=>  Location: PIN_R1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Y[0]	=>  Location: PIN_P2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- X[2]	=>  Location: PIN_P1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- X[1]	=>  Location: PIN_L9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Y[1]	=>  Location: PIN_N2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Y[2]	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF DMR IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_X : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_Y : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_M : std_logic;
SIGNAL ww_N : std_logic;
SIGNAL ww_P : std_logic;
SIGNAL \M~output_o\ : std_logic;
SIGNAL \N~output_o\ : std_logic;
SIGNAL \P~output_o\ : std_logic;
SIGNAL \Y[0]~input_o\ : std_logic;
SIGNAL \X[0]~input_o\ : std_logic;
SIGNAL \X[2]~input_o\ : std_logic;
SIGNAL \Y[2]~input_o\ : std_logic;
SIGNAL \Y[1]~input_o\ : std_logic;
SIGNAL \X[1]~input_o\ : std_logic;
SIGNAL \N~0_combout\ : std_logic;
SIGNAL \M~0_combout\ : std_logic;
SIGNAL \N~1_combout\ : std_logic;
SIGNAL \N~2_combout\ : std_logic;
SIGNAL \P~0_combout\ : std_logic;
SIGNAL \P~1_combout\ : std_logic;
SIGNAL \ALT_INV_M~0_combout\ : std_logic;

BEGIN

ww_X <= X;
ww_Y <= Y;
M <= ww_M;
N <= ww_N;
P <= ww_P;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_M~0_combout\ <= NOT \M~0_combout\;

-- Location: IOOBUF_X0_Y10_N2
\M~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_M~0_combout\,
	devoe => ww_devoe,
	o => \M~output_o\);

-- Location: IOOBUF_X9_Y0_N23
\N~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \N~2_combout\,
	devoe => ww_devoe,
	o => \N~output_o\);

-- Location: IOOBUF_X0_Y6_N16
\P~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \P~1_combout\,
	devoe => ww_devoe,
	o => \P~output_o\);

-- Location: IOIBUF_X0_Y4_N15
\Y[0]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Y(0),
	o => \Y[0]~input_o\);

-- Location: IOIBUF_X0_Y5_N22
\X[0]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_X(0),
	o => \X[0]~input_o\);

-- Location: IOIBUF_X0_Y4_N22
\X[2]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_X(2),
	o => \X[2]~input_o\);

-- Location: IOIBUF_X0_Y6_N22
\Y[2]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Y(2),
	o => \Y[2]~input_o\);

-- Location: IOIBUF_X0_Y7_N15
\Y[1]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Y(1),
	o => \Y[1]~input_o\);

-- Location: IOIBUF_X18_Y0_N1
\X[1]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_X(1),
	o => \X[1]~input_o\);

-- Location: LCCOMB_X1_Y4_N0
\N~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \N~0_combout\ = (\X[2]~input_o\ & (\Y[2]~input_o\ & (\Y[1]~input_o\ $ (!\X[1]~input_o\)))) # (!\X[2]~input_o\ & (!\Y[2]~input_o\ & (\Y[1]~input_o\ $ (!\X[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \X[2]~input_o\,
	datab => \Y[2]~input_o\,
	datac => \Y[1]~input_o\,
	datad => \X[1]~input_o\,
	combout => \N~0_combout\);

-- Location: LCCOMB_X1_Y4_N10
\M~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \M~0_combout\ = (\Y[0]~input_o\ $ (\X[0]~input_o\)) # (!\N~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Y[0]~input_o\,
	datac => \X[0]~input_o\,
	datad => \N~0_combout\,
	combout => \M~0_combout\);

-- Location: LCCOMB_X1_Y4_N12
\N~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \N~1_combout\ = (\X[2]~input_o\ & (((!\Y[1]~input_o\ & \X[1]~input_o\)) # (!\Y[2]~input_o\))) # (!\X[2]~input_o\ & (!\Y[2]~input_o\ & (!\Y[1]~input_o\ & \X[1]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \X[2]~input_o\,
	datab => \Y[2]~input_o\,
	datac => \Y[1]~input_o\,
	datad => \X[1]~input_o\,
	combout => \N~1_combout\);

-- Location: LCCOMB_X1_Y4_N6
\N~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \N~2_combout\ = (\N~1_combout\) # ((!\Y[0]~input_o\ & (\N~0_combout\ & \X[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Y[0]~input_o\,
	datab => \N~0_combout\,
	datac => \X[0]~input_o\,
	datad => \N~1_combout\,
	combout => \N~2_combout\);

-- Location: LCCOMB_X1_Y4_N24
\P~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \P~0_combout\ = (\X[2]~input_o\ & (\Y[2]~input_o\ & (\Y[1]~input_o\ & !\X[1]~input_o\))) # (!\X[2]~input_o\ & ((\Y[2]~input_o\) # ((\Y[1]~input_o\ & !\X[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010011010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \X[2]~input_o\,
	datab => \Y[2]~input_o\,
	datac => \Y[1]~input_o\,
	datad => \X[1]~input_o\,
	combout => \P~0_combout\);

-- Location: LCCOMB_X1_Y4_N26
\P~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \P~1_combout\ = (\P~0_combout\) # ((\Y[0]~input_o\ & (!\X[0]~input_o\ & \N~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Y[0]~input_o\,
	datab => \P~0_combout\,
	datac => \X[0]~input_o\,
	datad => \N~0_combout\,
	combout => \P~1_combout\);

ww_M <= \M~output_o\;

ww_N <= \N~output_o\;

ww_P <= \P~output_o\;
END structure;


